#!/usr/bin/env fish

for file in ~/dotfiles/shell/fish/utils/*.fish
    source $file
end
