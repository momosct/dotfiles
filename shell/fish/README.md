# Setup Fish

## Install Fish
`brew install fish`

## Setup Plugin Manager
Details: https://github.com/jorgebucaran/fisher

`curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher`
link fishfile to `~/.config/fish/fish_plugins`
restart fish shell
run `fisher update`
