#!/usr/bin/env sh

export dotfiles="$HOME/dotfiles"
export PATH="/usr/local/bin:/usr/local/sbin:$PATH"
